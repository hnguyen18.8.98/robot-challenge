
# PowerShell Robot Challenge Script

![PowerShell Version](https://img.shields.io/badge/PowerShell-5.1-blue.svg)
![Platform](https://img.shields.io/badge/Platform-Windows-green.svg)

This PowerShell script simulates a toy robot moving on a square tabletop, responding to specific commands while avoiding falling off the table.
## Psuedo Code

Below is an image explaining the logic used in the script:

![Pseudo Code Logic](psudo-code.jpg)

## Download the Game

You can download the game executable from the following link:
[Download Robot Challenge Game](https://resume.hnguyen-cloud.com/assets/Downloads/Robot-challenge/robot.exe)

View My Portfolios:
[My Resume](https://resume.hnguyen-cloud.com/)

Ensure you have the necessary permissions and system requirements to run executables on your system.

## Setting Up

Before running the script, you may need to adjust your PowerShell execution policy. This is a safety feature that controls the conditions under which PowerShell loads configuration files and runs scripts.

### Temporary Execution Policy Change - Don't need to do this if you follow Method 1 & 3

To temporarily change the execution policy for your current PowerShell session only (does not affect the system-wide setting), follow these steps:

1. **Open PowerShell as Administrator**.
2. Run the following command:

   ```powershell
   powershell.exe -ExecutionPolicy Bypass
   ```

   This command starts a new PowerShell session with the execution policy set to Bypass for that session only.

### Running the Script

After setting the execution policy, you can run the script in different ways:

#### Method 1: Using PowerShell ISE (My favourite - the simplest way)

- Open PowerShell ISE.
- Copy all the code from `Main.ps1` in my repository.
- Paste the code into the PowerShell ISE script pane.
- Press F5 or click the 'Run Script' button to execute the script.

#### Method 2: Run Directly in PowerShell

- Navigate to the directory containing the script.
- Execute the script directly:

  ```powershell
  .\Example.ps1
  ```

#### Method 3: Specifying the Script in the Command -- You don't need to pass ExecutionPolicy before if you follows this method

- Run the script by specifying its path in the command:

  ```powershell
  powershell.exe -ExecutionPolicy Bypass -File "\Path\To\Powershell\File\example.ps1"
  ```

   Replace `\Path\To\Powershell\File\example.ps1` with the actual path to your script.

#### Method 4: Right Click and Run with PowerShell

This method is less secure and not recommended. Make sure to revert the execution policy changes after running the script.

- To temporarily set your system's execution policy to Bypass, open PowerShell as an Administrator and run:
  ```powershell
  Set-ExecutionPolicy Bypass -Scope CurrentUser
  ```
- Remember, this lowers the security of your system. It's important to revert this change after running the script:
  ```powershell
  Set-ExecutionPolicy Restricted -Scope CurrentUser
  ```
- Once the execution policy is set, navigate to the folder containing `Main.ps1`.
- Right-click on `Main.ps1` and select 'Run with PowerShell'.

After running the script, ensure to revert the execution policy to its original setting for security reasons.

## Usage Instructions

After starting the script, you will be prompted to enter commands to control the robot. The available commands are:

- `PLACE X,Y,Z`: Places the robot on the table at coordinates X,Y facing direction Z (NORTH, EAST, SOUTH, WEST).
- `MOVE`: Moves the robot one unit forward in the direction it is currently facing.
- `LEFT`: Rotates the robot 90 degrees to the left without changing its position.
- `RIGHT`: Rotates the robot 90 degrees to the right without changing its position.
- `REPORT`: Announces the current X, Y, and Z of the robot.
- `EXIT`: Exits the script.
- `RESTART`: Restart the game

Ensure that the first command is a `PLACE` command to start the simulation.
