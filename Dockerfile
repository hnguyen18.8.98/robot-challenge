FROM m365pnp/powershell:2.2.142-nightly-alpine-3.14
WORKDIR /usr/src/app
COPY Main.ps1 .
CMD ["pwsh", "-File", "./Main.ps1"]

