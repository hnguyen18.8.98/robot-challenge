function MoveRobot {
    param (
        [int]$currentX,
        [int]$currentY,
        [int]$indicator
    )

    $canMove = $True

    switch ($indicator) {
        1 { # North
            if ($currentY -lt 4) {
                $currentY++
            } else {
                $canMove = $False
            }
        }
        2 { # East
            if ($currentX -lt 4) {
                $currentX++
            } else {
                $canMove = $False
            }
        }
        3 { # South
            if ($currentY -gt 0) {
                $currentY--
            } else {
                $canMove = $False
            }
        }
        4 { # West
            if ($currentX -gt 0) {
                $currentX--
            } else {
                $canMove = $False
            }
        }
    }

    return $currentX, $currentY, $canMove
}

# Function to convert direction string to indicator
function ConvertDirectionToIndicator($direction) {
    switch ($direction) {
        "NORTH" { return 1 }
        "EAST"  { return 2 }
        "SOUTH" { return 3 }
        "WEST"  { return 4 }
    }
}

# Function to convert indicator back to direction string
function ConvertIndicatorToDirection($indicator) {
    switch ($indicator) {
        1 { return "NORTH" }
        2 { return "EAST" }
        3 { return "SOUTH" }
        4 { return "WEST" }
    }
}

#Game intro, run once to prevent looping if playing the same session
Write-Host "WELCOME TO THE ROBOT CHALLENGE" -ForegroundColor Blue
Write-Host "A new game session has started. Please try not to damage the robot by moving it off the table." -ForegroundColor Yellow

function StartGame {
    $isPlaced = $False # Check if the robot has been placed on the table
	### Default variables, get updated on PLACE command
    $currentX = 0
    $currentY = 0
    $currentDirection = "NORTH"


    # Loop until a valid PLACE command is entered
    Write-Host "You can type " -NoNewline
    Write-Host "'EXIT' " -ForegroundColor Red -NoNewline
    Write-Host "to quit the game at any time or " -NoNewline
    Write-Host "'RESTART' " -ForegroundColor Green -NoNewline
    Write-Host "to start a new session"
    while (-not $isPlaced) {
        Write-Host "Enter the first command (PLACE X,Y,Z)" -ForegroundColor Yellow
        $command = Read-Host
        # Check if the user wants to exit the game
        if ($command -eq "exit") {
            exit
        }
        if ($command -eq "restart") {
            $isPlaced = $False
            return
        }

        # Split the command into parts
        $commandParts = $command -split ' |,'
        $commandName = $commandParts[0]
        $x = $commandParts[1]
        $y = $commandParts[2]
        $z = $commandParts[3]

        # check for valid PLACE command
        if ($commandName -eq "PLACE" -and $x -ge 0 -and $x -le 4 -and $y -ge 0 -and $y -le 4 -and "NORTH","SOUTH","EAST","WEST" -contains $z) {
            $currentX = [int]$x
            $currentY = [int]$y
            $currentDirection = $z
            $isPlaced = $True
        }
        else {
            Write-Host "Invalid 1st command. " -ForegroundColor Red -NoNewline
            Write-Host "Please place the robot on the table first by entering " -NoNewline
            Write-Host "'PLACE' " -ForegroundColor Magenta -NoNewline
            Write-Host "followed by coordinates within " -NoNewline
            Write-Host "0 to 4 and direction (NORTH, SOUTH, EAST, or WEST)" -ForegroundColor DarkCyan
        }
    }

    #### Only runs after 1st command is valid (Robot is placed)
    while ($isPlaced) {
        Write-Host "Enter a command for robot to perform. You can choose from MOVE, LEFT, RIGHT, REPORT," -NoNewline -ForegroundColor Green
        Write-Host " or PLACE it to another square:" -ForegroundColor Cyan
        $command = Read-Host
        # Check if the user wants to exit the game
        if ($command -eq "exit") {
            exit
        }

        if ($command -eq "restart") {
            $isPlaced = $False
            return
        }
        
        if ($command -eq "REPORT") {
            Write-Host "Reporting: X=$currentX, Y=$currentY, Facing=$currentDirection" -ForegroundColor Magenta
            
            # Ask if the user wants to continue playing
            while ($True) {
                Write-Host "Do you want to keep playing the current game (yes/no)?" -ForegroundColor Yellow
                $continuePlaying = Read-Host
                if ($continuePlaying -eq "no") {
                    Write-Host "Starting another game in the same session, please place the robot again" -ForegroundColor Blue
                    $isPlaced = $False
                    return
                }
                elseif ($continuePlaying -eq "yes") {
                    break
                }
                else {
                    Write-Host "Please answer 'yes' or 'no'." -ForegroundColor Red
                }
            }
        }
        # Check for other commands like MOVE, LEFT, RIGHT, or new PLACE
        elseif ($command -eq "MOVE") {
            $indicator = ConvertDirectionToIndicator $currentDirection
            $moveResult = MoveRobot $currentX $currentY $indicator
            $currentX, $currentY, $canMove = $moveResult[0], $moveResult[1], $moveResult[2]
            if (-not $canMove) {
                Write-Host "You can't move this direction. Please consider changing direction with " -ForegroundColor Red -NoNewline
                Write-Host "LEFT, RIGHT " -ForegroundColor Yellow -NoNewline
                Write-Host "commands." -ForegroundColor Red
            }
        }
        elseif ($command -eq "LEFT") {
            $indicator = ConvertDirectionToIndicator $currentDirection
            $indicator--

            if ($indicator -lt 1) {
                $indicator = 4 #turn to WEST if facing NORTH
            }

            $currentDirection = ConvertIndicatorToDirection $indicator
        }
        elseif ($command -eq "RIGHT") {
            $indicator = ConvertDirectionToIndicator $currentDirection
            $indicator++

            if ($indicator -gt 4) {
                $indicator = 1 # Return to NORTH if currently facing WEST
            }

            $currentDirection = ConvertIndicatorToDirection $indicator
        }
        elseif ($command.ToLower().StartsWith("place ")) { #Use ToLower() method to compare since StartWith method is case sensitive
            ## split new command up to check
            $commandParts = $command -split ' |,'
            $x = $commandParts[1]
            $y = $commandParts[2]
            $z = $commandParts[3]

            # check for valid PLACE command
            if ($x -ge 0 -and $x -le 4 -and $y -ge 0 -and $y -le 4 -and "NORTH","SOUTH","EAST","WEST" -contains $z) {
                $currentX = [int]$x
                $currentY = [int]$y
                $currentDirection = $z
            }
            else {
                Write-Host "Invalid PLACE command. " -ForegroundColor Red -NoNewline
                Write-Host "Please place the robot on the table first by entering " -NoNewline
                Write-Host "PLACE " -ForegroundColor Magenta -NoNewline
                Write-Host "followed by coordinates within " -NoNewline
                Write-Host "0 to 4 and direction (NORTH, SOUTH, EAST, or WEST)" -ForegroundColor DarkCyan
            }
        }
        else {
            Write-Host "Invalid command." -ForegroundColor Red
        }
    }
}

# Start the game, and keep it running until the user chooses to exit
do {
    StartGame
} while ($True)
